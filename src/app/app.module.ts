import { ServiceWorkerModule } from '@angular/service-worker'
import { BrowserModule } from '@angular/platform-browser'
import { LOCALE_ID, NgModule } from '@angular/core'
import { RouteReuseStrategy } from '@angular/router'
import { registerLocaleData } from '@angular/common'
import localeFr from '@angular/common/locales/fr'

import { IonicModule, IonicRouteStrategy } from '@ionic/angular'
import { SplashScreen } from '@ionic-native/splash-screen/ngx'
import { StatusBar } from '@ionic-native/status-bar/ngx'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'

import { environment } from '../environments/environment'

registerLocaleData(localeFr)
// { mode: 'md' }

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    IonicModule.forRoot(),
    ServiceWorkerModule.register('ngsw-worker.js', { 
      enabled: environment.production 
    })
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { 
      provide: LOCALE_ID, 
      useValue: "fr" 
    },
    { 
      provide: RouteReuseStrategy, 
      useClass: IonicRouteStrategy 
    }
  ],
  bootstrap: [
    AppComponent
  ]
})

export class AppModule {}
