import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
  name: 'zeroPadding'
})

export class ZeroPaddingPipe implements PipeTransform 
{

   transform(value:number): string
   {
      if( value != undefined )
         return ('0' + value).slice(-2)
   }

}