import { NgModule } from '@angular/core'
import { RangePipe } from './range/range.pipe'
import { ZeroPaddingPipe } from './zeroPadding/zeroPadding.pipe'

@NgModule({
   declarations: [
      RangePipe,
      ZeroPaddingPipe
   ],
   exports: [
      RangePipe,
      ZeroPaddingPipe
   ]
})

export class PipesModule { }
