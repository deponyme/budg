import { Component, AfterViewInit } from '@angular/core'
import { ModalController } from '@ionic/angular'

import { CalendarService, CalendarCell } from '../../services/calendar/calendar.service'
import { SpendingsService, Spending } from '../../services/spendings/spendings.service'
import { EarningsService, Earning } from '../../services/earnings/earnings.service'
import { LeviesService, Levy } from '../../services/levies/levies.service'
import { CoreService } from '../../services/core/core.service'

import { CellDetailsComponent } from './components/cell-details/cell-details.component'

@Component({
   selector    : 'app-calendar',
   templateUrl : './calendar.page.html'
})
export class CalendarPage implements AfterViewInit
{
   public headerLetters: string[] = this.calendarService.headerLetters
   public calendarRows: Array<CalendarCell[]> = []

   private monthSpendings: Spending[] = []
   private monthEarnings: Earning[] = []
   private monthLevies: Levy[] = []

   constructor(
      private spendingsService: SpendingsService,
      private calendarService: CalendarService,
      private earningsService: EarningsService,
      private modalController: ModalController,
      private leviesService: LeviesService,
      private coreService: CoreService
   ){}

   ngAfterViewInit()
   {
      this.setCalendarContent()
      this.setSubscribers()      
   }

   /**
   * Set subscribers callback
   */
   private setSubscribers = ():void => 
   {
      this.coreService.currentDateChange.subscribe( () => this.setCalendarContent() )
      this.spendingsService.spendingsChanged.subscribe( () => this.setCalendarContent() )
      this.earningsService.earningsChanged.subscribe( () => this.setCalendarContent() )
      this.leviesService.leviesChanged.subscribe( () => this.setCalendarContent() )
   }

   /**
   * Check if cell is today cell
   * @param {CalendarCell} 
   * @return {Boolean} - True is cell is today | false cell is not
   */
   public cellDateIsToday = (cell:CalendarCell):boolean =>
   {
      if( cell.date == null )
         return false

      return cell.date.getTime() == new Date().setHours(0,0,0,0)
   }

   /**
   * Display cell details ( spendings / earnings / levies ) modal
   * @param {CalendarCell} 
   */
   public presentCellDetails = async (cell:CalendarCell):Promise<void> =>
   {
      const modal = await this.modalController.create({
         component: CellDetailsComponent,
         componentProps: { cell }
      })
      
      return await modal.present()
   }

   /**
   * Get array of spendings object for displayed core month
   * @return {Promise<Spending[]>}
   */
   private getMonthSpendings = ():Promise<Spending[]> =>
   {
      return new Promise( (resolve):void => 
      {
         const coreMonth = this.coreService.getCurrentMonth()
         const coreYear = this.coreService.getCurrentYear()

         this.spendingsService.getForMonth(coreMonth, coreYear).then( (spendings:Spending[]):void => 
         {
            resolve(spendings)
         })          
      })
   }

   /**
   * Get array of earnings object for displayed core month
   * @return {Promise<Earning[]>}
   */
   private getMonthEarnings = ():Promise<Earning[]> =>
   {
      return new Promise( (resolve):void => 
      {
         const coreMonth = this.coreService.getCurrentMonth()
         const coreYear = this.coreService.getCurrentYear()

         this.earningsService.getForMonth(coreMonth, coreYear).then( (earnings:Earning[]):void => 
         {
            resolve(earnings)
         })          
      })
   }

   /**
   * Get array of levies object for displayed core month
   * @return {Promise<Levy[]>}
   */
   private getMonthLevies = ():Promise<Levy[]> =>
   {
      return new Promise( (resolve):void => 
      {
         const coreMonth = this.coreService.getCurrentMonth()
         const coreYear = this.coreService.getCurrentYear()

         this.leviesService.getForMonth(coreMonth, coreYear).then( (levies:Levy[]):void => 
         {
            resolve(levies)
         })          
      })
   }

   /**
   * Test if cell contain an entity ( earning/spendings )
   * @param {String} - entity searched : 'earning' | 'spending'
   * @param {CalendarCell} - tested cell
   * @return {Boolean} - true if cell contain entity false if not
   */
   public cellContain = (entity:string, cell:CalendarCell):boolean => 
   {
      if( cell.date != null )
         switch (entity) {
            case "earnings":
               return this.monthEarnings.filter( (earning:Earning) => {
                  return earning.date.getTime() == cell.date.getTime()
               }).length > 0
               break;
            case "spendings":
               return this.monthSpendings.filter( (spending:Spending) => {
                  return spending.date.getTime() == cell.date.getTime()
               }).length > 0
            case "levies":
               return this.monthLevies.filter( (levy:Levy) => {
                  return levy.day == cell.date.getDate()
               }).length > 0
            default:
               return false
               break;
         }

      return false
   }

   /**
   * Set calendar's datas ( earnings, levies, spendings )
   */
   private setCalendarContent = async ():Promise<void> =>
   {
      this.monthSpendings = await this.getMonthSpendings()
      this.monthEarnings = await this.getMonthEarnings()
      this.monthLevies = await this.getMonthLevies()

      this.calendarRows = this.calendarService.getCalendarArray()
   }
}
