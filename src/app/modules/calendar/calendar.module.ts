// Angular
import { Routes, RouterModule } from '@angular/router'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'
import { NgModule } from '@angular/core'

// Ionic
import { IonicModule } from '@ionic/angular'

// Customs
import { AppCommonModule } from './../../components/app-common.module'
import { CalendarPage } from './calendar.page'

import { CellDetailsComponent } from './components/cell-details/cell-details.component'


// Router
const routes: Routes = [
  {
    path: '',
    component: CalendarPage
  }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AppCommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    CalendarPage,
    CellDetailsComponent
  ],
  entryComponents:[
    CellDetailsComponent
  ]
})
export class CalendarPageModule {}
