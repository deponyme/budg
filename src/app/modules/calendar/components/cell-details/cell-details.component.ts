import { Component, OnInit, Input } from '@angular/core'
import { ModalController } from '@ionic/angular'

import { SpendingsService, Spending } from '../../../../services/spendings/spendings.service'
import { EarningsService, Earning } from '../../../../services/earnings/earnings.service'
import { LeviesService, Levy } from '../../../../services/levies/levies.service'
import { CalendarCell } from '../../../../services/calendar/calendar.service'

import { SpendingFormComponent } from '../../../../components/spending-form/spending-form.component'
import { EarningFormComponent } from '../../../../components/earning-form/earning-form.component'
import { LevyFormComponent } from '../../../../components/levy-form/levy-form.component'

@Component({
   selector    : 'calendar-cell-details',
   templateUrl : './cell-details.component.html'
})

export class CellDetailsComponent implements OnInit 
{
   @Input() cell : CalendarCell

   public spendings : Spending[] = []
   public earnings : Earning[] = []
   public levies : Levy[] = []

   constructor(
      private spendingsService: SpendingsService,
      private earningsService: EarningsService,
      private modalController: ModalController,
      private leviesService: LeviesService
   ){}

   ngOnInit() {
      this.setEarnings()
      this.setSpendings()
      this.setLevies()

      this.setSubscribers()
   }

   /**
   * Set subscribers for update earnings & spendings
   */
   private setSubscribers = (): void =>
   {
      this.earningsService.earningsChanged.subscribe( (): void => 
      {
         this.setEarnings() 
      })

      this.spendingsService.spendingsChanged.subscribe( (): void =>
      {
         this.setSpendings() 
      })

      this.leviesService.leviesChanged.subscribe( (): void => 
      {
         this.setLevies() 
      })
   }

   /**
   * Display earning form modal
   * @param {Earning}
   */
   public presentEarningFormModal = async (earning: Earning = new Earning()): Promise<void> => 
   {
      if( earning.id == null )
         earning.date = this.cell.date

      const modal = await this.modalController.create({
         component: EarningFormComponent,
         componentProps: { earning }
      })
      
      return await modal.present()
   }

   /**
   * Display spending form modal
   * @param {Spending}
   */
   public presentSpendingFormModal = async (spending: Spending = new Spending()): Promise<void> => 
   {
      if( spending.id == null )
         spending.date = this.cell.date

      const modal = await this.modalController.create({
         component: SpendingFormComponent,
         componentProps: { spending }
      })
      
      return await modal.present()
   }

   /**
   * Display levy form modal
   * @param {Levy}
   */
   public presentLevyModal = async (levy: Levy = new Levy()): Promise<void> => 
   {
      const modal = await this.modalController.create({
         component: LevyFormComponent,
         componentProps: { levy }
      })
      
      return await modal.present()
   }

   /**
   * Dismiss this modal
   */
   public dismissModal = (): void =>
   {
      this.modalController.dismiss()
   }

   /**
   * Set earning's cell
   */
   private setEarnings = async():Promise<void> =>
   {
      this.earnings = await this.earningsService.getForDay(this.cell.date)
   }

   /**
   * Get & Set spendings for cell
   */
   private setSpendings = async():Promise<void> =>
   {
      this.spendings = await this.spendingsService.getForDay(this.cell.date)
   }

   /**
   * Get & Set levies for cell
   */
   private setLevies = async():Promise<void> =>
   {
      this.levies = await this.leviesService.getForDay(this.cell.date)
   }

}
