import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'
import { Routes, RouterModule } from '@angular/router'

import { IonicModule } from '@ionic/angular'

import { AppCommonModule } from '../../components/app-common.module'

import { CloudPage } from './cloud.page'

const routes: Routes = [
  {
    path: '',
    component: CloudPage
  }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AppCommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CloudPage]
})
export class CloudPageModule {}
