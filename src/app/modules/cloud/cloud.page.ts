import { Component, OnInit } from '@angular/core'
import { UserService } from './../../services/user/user.service'

@Component({
  selector: 'app-cloud',
  templateUrl: './cloud.page.html'
})

export class CloudPage implements OnInit 
{

  constructor(
    private userService: UserService
  ) { }

  ngOnInit() {
  }

  public userIsLogged = (): boolean => 
  {
    return this.userService.userIsLogged()
  }

}
