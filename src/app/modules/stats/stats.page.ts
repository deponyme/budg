import { Component, AfterViewInit } from '@angular/core'
import { ModalController } from '@ionic/angular'

import { SpendingsService, Spending } from '../../services/spendings/spendings.service'
import { FoldersService, Folder } from '../../services/folders/folders.service'
import { EarningsService } from '../../services/earnings/earnings.service'
import { LeviesService, Levy } from '../../services/levies/levies.service'
import { StatsService, Stats } from '../../services/stats/stats.service'
import { CoreService } from '../../services/core/core.service'

import { StatsDetailsComponent } from './components/stats-details/stats-details.component'

@Component({
   selector: 'app-stats',
   templateUrl: './stats.page.html'
})

export class StatsPage implements AfterViewInit 
{
   public stats: Stats = new Stats()

   constructor(
      private spendingsService: SpendingsService,
      private earningsService: EarningsService,
      private modalController: ModalController,
      private foldersService: FoldersService,
      private leviesService: LeviesService,
      private statsService: StatsService,
      private coreService: CoreService
   ) {}

   async ngAfterViewInit()
   {
      this.stats = await this.statsService.getStats()

      this.setSubscribers()
   }

   /**
   * Set subscribers callback
   */
   private setSubscribers = ():void => 
   {
      this.coreService.currentDateChange.subscribe( async() => this.stats = await this.statsService.getStats() )
      this.spendingsService.spendingsChanged.subscribe( async() => this.stats = await this.statsService.getStats() )
      this.earningsService.earningsChanged.subscribe( async() => this.stats = await this.statsService.getStats() )
      this.leviesService.leviesChanged.subscribe( async() => this.stats = await this.statsService.getStats() )
   }


   /**
   * Display stats details modal
   * @param {string} type : earnings | spendings | levies | folder
   * @param {folder} 
   */
   public presentStatsDetails = async (type: string, folder: Folder = null):Promise<void> =>
   {
      const modal = await this.modalController.create({
         component: StatsDetailsComponent,
         componentProps: { 
            type: type,
            folder: folder
         }
      })
      
      return await modal.present()
   }

}
