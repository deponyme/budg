// Angular
import { Routes, RouterModule } from '@angular/router'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'
import { NgModule } from '@angular/core'

// Ionic
import { IonicModule } from '@ionic/angular'

// Customs
import { StatsPage } from './stats.page'
import { AppCommonModule } from '../../components/app-common.module'
import { StatsDetailsComponent } from './components/stats-details/stats-details.component'


const routes: Routes = [
  {
    path: '',
    component: StatsPage
  }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AppCommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    StatsPage,
    StatsDetailsComponent
  ],
  entryComponents: [
    StatsDetailsComponent
  ]
})

export class StatsPageModule {}
