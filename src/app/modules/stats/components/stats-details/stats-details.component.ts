import { Component, OnInit, Input } from '@angular/core'
import { ModalController } from '@ionic/angular'

import { SpendingsService, Spending } from '../../../../services/spendings/spendings.service'
import { EarningsService, Earning } from '../../../../services/earnings/earnings.service'
import { LeviesService, Levy } from '../../../../services/levies/levies.service'
import { StatsService, Stats } from '../../../../services/stats/stats.service'

import { SpendingFormComponent } from '../../../../components/spending-form/spending-form.component'
import { EarningFormComponent } from '../../../../components/earning-form/earning-form.component'
import { LevyFormComponent } from '../../../../components/levy-form/levy-form.component'
import { CoreService } from '../../../../services/core/core.service'
import { Folder } from '../../../../services/folders/folders.service'

@Component({
   selector    : 'stats-stats-details',
   templateUrl : './stats-details.component.html'
})

export class StatsDetailsComponent implements OnInit 
{
   public earnings: Earning[] = []
   public spendings: Spending[] = []
   public levies: Levy[] = []

   public title: string = null
   public date : Date = null

   //earnings | spendings | levies | folder
   @Input() type: string = null 

   @Input() folder: Folder
 
   constructor(
      private spendingsService: SpendingsService,
      private earningsService: EarningsService,
      private modalController: ModalController,
      private leviesService: LeviesService,
      private statsService: StatsService,
      private coreService: CoreService
   ){}

   async ngOnInit() 
   {
      this.setSubscribers()
      this.date = this.coreService.getCurrentDate()

      this.setStats()
   }

   /**
   * Set subscribers for update earnings & spendings
   */
   private setSubscribers = (): void =>
   {
      this.earningsService.earningsChanged.subscribe( ()  => 
      {
         this.setStats()
      })

      this.spendingsService.spendingsChanged.subscribe( ()  =>
      {
         this.setStats()
      })

      this.leviesService.leviesChanged.subscribe( ()  => 
      {
         this.setStats()
      })
   }

   /**
   * Get all aviable stats
   * @return {Promise<Stats>}
   */
   private setStats = async(): Promise<void> =>
   {
      const stats: Stats = await this.statsService.getStats()
      
      this.statsFilter(stats)

      return
   }

   /**
   * Data's filter for input type ( earnings | spendings | levies | folder )
   */
   private statsFilter = (stats: Stats): void => 
   {
      switch (this.type) {
         case "earnings":
            this.title = "Gains sur"
            this.earnings = stats.earnings
            break;
         case "spendings":
            this.title = "Dépenses sur"
            this.spendings = stats.spendings
            break;
         case "levies":
            this.title = "Prélèvements sur"
            this.levies = stats.levies
            break;
         case "folder":
            this.title = this.folder.title+" sur"
            
            this.spendings = stats.spendings.filter( (spending: Spending): boolean => 
            {
               return spending.folder_id == this.folder.id
            })

            this.levies = stats.levies.filter( (levy: Levy): boolean => 
            {
               return levy.folder_id == this.folder.id
            })
            break;
         default:
            // code...
            break;
      }

   } 

   /**
   * Display earning form modal
   * @param {Earning}
   */
   public presentEarningFormModal = async (earning: Earning): Promise<void> => 
   {
      const modal = await this.modalController.create({
         component: EarningFormComponent,
         componentProps: { earning }
      })
      
      return await modal.present()
   }

   /**
   * Display spending form modal
   * @param {Spending}
   */
   public presentSpendingFormModal = async (spending: Spending): Promise<void> => 
   {
      const modal = await this.modalController.create({
         component: SpendingFormComponent,
         componentProps: { spending }
      })
      
      return await modal.present()
   }

   /**
   * Display levy form modal
   * @param {Levy}
   */
   public presentLevyModal = async (levy: Levy): Promise<void> => 
   {
      const modal = await this.modalController.create({
         component: LevyFormComponent,
         componentProps: { levy }
      })
      
      return await modal.present()
   }

   /**
   * Dismiss this modal
   */
   public dismissModal = (): void =>
   {
      this.modalController.dismiss()
   }

}
