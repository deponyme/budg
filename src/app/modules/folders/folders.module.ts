import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { Routes, RouterModule } from '@angular/router'
import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'

import { IonicModule } from '@ionic/angular'

import { FolderFormComponent } from './components/folder-form/folder-form.component'
import { AppCommonModule } from './../../components/app-common.module'

import { FoldersPage } from './folders.page'

const routes: Routes = [
  {
    path: '',
    component: FoldersPage
  }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AppCommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    FoldersPage,
    FolderFormComponent
  ],
  entryComponents: [
    FolderFormComponent
  ]
})
export class FoldersPageModule {}
