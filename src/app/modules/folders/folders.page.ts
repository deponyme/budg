import { Component, AfterViewInit } from '@angular/core'

import { ModalController } from '@ionic/angular'

import { FolderFormComponent } from './components/folder-form/folder-form.component'
import { FoldersService, Folder } from './../../services/folders/folders.service'

@Component({
   selector    : 'app-folders',
   templateUrl : './folders.page.html'
})
export class FoldersPage implements AfterViewInit 
{
   public folders: Folder[] = []

   constructor(
      private modalController: ModalController,
      private foldersService: FoldersService
   ){}

   ngAfterViewInit() 
   {
      this.setFolders()

      this.foldersService.foldersChanged.subscribe( () => 
      { 
         this.setFolders() 
      })
   }

   /**
   * Set Folder Array
   */
   private setFolders = async(): Promise<void> =>
   {
      this.folders = await this.foldersService.get()
   }

   /**
   * Display folder form modal
   * @param {Folder}
   */
   public presentFolderModal = async (folder: Folder = new Folder() ): Promise<void> =>
   {
      const modal = await this.modalController.create({
         component: FolderFormComponent,
         componentProps: { folder }
      })
      
      return await modal.present()
   }

}
