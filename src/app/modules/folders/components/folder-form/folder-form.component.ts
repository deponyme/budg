import { Component, OnInit, Input } from '@angular/core'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'

import { ToastController, AlertController, ModalController } from '@ionic/angular'

import { FoldersService, Folder } from './../../../../services/folders/folders.service'
import { CoreService } from './../../../../services/core/core.service'

@Component({
   selector: 'folders-folder-form',
   templateUrl: './folder-form.component.html'
})
export class FolderFormComponent implements OnInit {

   @Input() folder: Folder
   public folderForm: FormGroup

   constructor(
      private toastController: ToastController,
      private alertController: AlertController,
      private modalController: ModalController,
      private foldersService: FoldersService,
      private coreService: CoreService,
      private formBuilder: FormBuilder
   ) { }

   ngOnInit() 
   {
      this.folderForm = this.formBuilder.group(this.folder)
   }

   /**
   * Create FormGroup for earning form
   * @return { FormGroup }
   */
   private getFormGroup = (): FormGroup =>
   {
      return this.formBuilder.group({
         title: [ 
            this.folder.title, 
            Validators.required 
         ]
      })
   }

   /**
   * Delete this earning
   */
   private deleteFolder = async(): Promise<void> =>
   {
      const deleted = await this.foldersService.delete(this.folder)

      if( deleted )
         this.coreService.presentToast('success', 'Le groupe a bien été supprimé.')
      else
         this.coreService.presentToast('error', 'Erreur : Le groupe n\'a pas pu être supprimé.')

      this.dismissModal()
   }

   /**
   * Presente delete alert choice. 
   * If user confirm, delete folder & dismiss modal
   */
   public presentFolderAlert = async(): Promise<void> => 
   {
      const alert = await this.alertController.create({
         header: 'Attention !',
         message: '<strong>La suppression est irréversible</strong>. Êtes-vous sûr de vouloir supprimer ce groupe de dépense définitivement ?',
         buttons: [
            {
               text: 'Annuler',
               role: 'cancel'
            }, {
               text: 'Supprimer',
               handler: () => this.deleteFolder()
            }
         ]
      })

      await alert.present()
   }

   /**
   * Dismiss modal
   */
   public dismissModal = (): void =>
   {
      this.modalController.dismiss()
   }

   /**
   * Presente delete alert choice. 
   * If user confirm, delete earning & dismiss modal
   */
   public async presentDeleteAlert() {
      const alert = await this.alertController.create({
         header: 'Attention !',
         message: 'Êtes-vous sûr de vouloir <strong>supprimer ce groupe de dépense ?</strong>',
         buttons: [
            {
               text: 'Annuler',
               role: 'cancel'
            }, {
               text: 'Supprimer',
               handler: () => this.deleteFolder()
            }
         ]
      })

      await alert.present()
   }

   /**
   * Save folder form 
   */
   public submit = async(): Promise<void> => 
   {
      Object.assign(this.folder, this.folderForm.value)

      const saved = await this.foldersService.save(this.folder)

      if( saved )
         this.coreService.presentToast('success', 'Le gain a bien été pris en compte.')
      else 
         this.coreService.presentToast('error', 'Erreur : Le gain n\'a pas pu être enregistré.')

      this.dismissModal()
   }
}
