import { Component, AfterViewInit } from '@angular/core'

import { ModalController } from '@ionic/angular'

import { LeviesService, Levy } from './../../services/levies/levies.service'
import { CoreService } from './../../services/core/core.service'

import { LevyFormComponent } from './../../components/levy-form/levy-form.component'

@Component({
   selector    : 'app-levies',
   templateUrl : './levies.page.html'
})

export class LeviesPage implements AfterViewInit 
{
   public levies: Levy[] = []
   public displayedMonth: Date 

   constructor(
      private modalController: ModalController,
      private leviesService: LeviesService,
      private coreService: CoreService
   ){}

   async ngAfterViewInit() 
   {
      this.displayedMonth = this.coreService.getCurrentDate()
      
      this.setLevies()
      this.setSubscribers()
   }

   /**
   * Set subscribers callbacks
   */
   private setSubscribers = (): void => 
   {
      this.coreService.currentDateChange.subscribe( ():void => 
      {
         this.displayedMonth = this.coreService.getCurrentDate()

         this.setLevies() 
      })

      this.leviesService.leviesChanged.subscribe( ():void => 
      {
         this.setLevies()
      })
   }

   /**
   * Set Levy Array for current core month 
   */
   private setLevies = async(): Promise<void> =>
   {
      const coreMonth = this.coreService.getCurrentMonth()
      const coreYear = this.coreService.getCurrentYear()

      this.levies = await this.leviesService.getForMonth(coreMonth, coreYear)
   }

   /**
   * Display levy form modal
   * @param {Levy}
   */
   public presentLevyModal = async(levy: Levy = new Levy()): Promise<void> =>
   {
      const modal = await this.modalController.create({
         component: LevyFormComponent,
         componentProps: { levy }
      })
      
      return await modal.present()
   }
   
}
