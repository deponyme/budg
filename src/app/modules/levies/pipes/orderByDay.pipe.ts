import { Pipe, PipeTransform } from '@angular/core'

import { Levy } from './../../../services/levies/levies.service'

@Pipe({
  name: 'orderByDay'
})

export class OrderByDayPipe implements PipeTransform 
{

   transform(value: Array<Levy>): Array<Levy>
   {
      if( value != undefined )
         return value.sort( (a: Levy, b: Levy): number => 
         {
            if( a.day < b.day )
               return -1
            
            if( a.day > b.day )
               return 1
             
            return 0
         })
   }
   
}
