// Angular
import { Routes, RouterModule } from '@angular/router'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'
import { NgModule } from '@angular/core'

// Ionic
import { IonicModule } from '@ionic/angular'

// Customs
import { AppCommonModule } from './../../components/app-common.module'
import { PipesModule } from './../../pipes/pipes.module'

import { OrderByDayPipe } from './pipes/orderByDay.pipe'
import { LeviesPage } from './levies.page'

// Router
const routes: Routes = [
  {
    path: '',
    component: LeviesPage
  }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AppCommonModule,
    PipesModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    LeviesPage,
    OrderByDayPipe
  ]
})
export class LeviesPageModule {}
