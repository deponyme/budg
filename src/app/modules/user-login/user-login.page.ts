import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { Component, OnInit } from '@angular/core'
import { AlertController } from '@ionic/angular'

import { UserService, User } from './../../services/user/user.service'

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.page.html',
})
export class UserLoginPage implements OnInit 
{
  public loginForm: FormGroup
  private user: User = new User() 

  constructor(
    private alertController: AlertController,
    private userService: UserService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.loginForm = this.getFormGroup()
  }

  private getFormGroup = (): FormGroup =>
  {
    return this.formBuilder.group({
      email: [ 
        this.user.email, 
        Validators.required 
      ],
      password: [
        this.user.password, 
        Validators.required 
      ]
    })
  }
}
