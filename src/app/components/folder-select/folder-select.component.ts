import { Component, OnInit, Input } from '@angular/core'

import { FoldersService, Folder } from '../../services/folders/folders.service'
import { FormGroup } from '@angular/forms'

@Component({
   selector: 'common-folder-select',
   templateUrl: './folder-select.component.html',
})

export class FolderSelectComponent
{
   @Input() formGroup: FormGroup

   public folders: Folder[] = []
   public isInit: boolean = false

   constructor(
      private foldersService : FoldersService
   ){}

   async ngOnInit()
   {
      this.folders = await this.foldersService.get()
      this.folders.push(this.foldersService.getUnclassifiedFolder())

      if( this.formGroup.get('folder_id').value != null )
      {
         const selectValue:number = parseInt(this.formGroup.get('folder_id').value)

         const selectValueExistOnFolders: boolean = this.folders.filter( (folder:Folder):boolean => {
            return folder.id == selectValue
         }).length > 0

         if( !selectValueExistOnFolders )
         {
            const deletedFolder: Folder[] = await this.foldersService.get(true) 
         
            const folder: Folder = deletedFolder
               .filter( (folder:Folder):boolean => {
                  return folder.id == selectValue
               })
               .map( (folder: Folder): Folder => {
                  folder.title += ' ( supprimé )'

                  return folder
               })[0]

            this.folders.unshift(folder)
         }
      }

      this.isInit = true
   }
   
}
