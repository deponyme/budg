import { Component, OnInit } from '@angular/core'

import { CoreService } from '../../services/core/core.service'

@Component({
   selector    : 'common-month-selector',
   templateUrl : './month-selector.component.html'
})
export class MonthSelectorComponent implements OnInit 
{
   public date : Date = null

   constructor(
      private coreService: CoreService
   ){}

   /**
   * Set current core date displayable on picker
   * Subscribe to any current core date changes to set picker date
   */
   ngOnInit() 
   {
      this.date = this.coreService.getCurrentDate()
   }

   /**
   * Add/remove 1 month to displayed date
   * @param {string} value - 'Next'|'Previous'
   */
   public setMonthTo = (value:string): void =>
   {
      switch (value) {
         case "next":
            this.date = new Date(this.date.setMonth(this.date.getMonth()+1))
            break;
         case "previous":
            this.date = new Date(this.date.setMonth(this.date.getMonth()-1))
            break;
      }

      this.updateCoreDate(this.date)
   }

   /**
   * Update coreService current date
   * @param {Date}
   */
   private updateCoreDate = (date:Date): void =>
   {
      this.coreService.setCurrentDate(date)
   }
   
}
