import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { AlertController, ModalController } from '@ionic/angular'
import { Component, OnInit, Input } from '@angular/core'

import { EarningsService, Earning } from './../../services/earnings/earnings.service'
import { CoreService } from './../../services/core/core.service'

@Component({
   selector    : 'common-earning-form',
   templateUrl : './earning-form.component.html'
})

export class EarningFormComponent implements OnInit {

   @Input() earning : Earning
   
   public earningForm : FormGroup

   constructor(
      private earningsService: EarningsService,
      private alertController: AlertController,
      private modalController: ModalController,
      private coreService: CoreService,
      private formBuilder: FormBuilder
   ){}

   ngOnInit() 
   {
      this.earningForm = this.getFormGroup()
   }

   /**
   * Create FormGroup for earning form
   * @return { FormGroup }
   */
   private getFormGroup = (): FormGroup =>
   {
      return this.formBuilder.group({
         title: [ 
            this.earning.title, 
            Validators.required 
         ],
         amount : [
            this.earning.amount, 
            [ 
               Validators.required, 
               Validators.min(0.01),
               Validators.pattern(/^[0-9]+(\.[0-9]{1,2})?$/)
            ]
         ]
      })
   }

   /**
   * Delete this earning
   */
   private deleteEarning = async(): Promise<void> =>
   {
      const deleted = await this.earningsService.delete(this.earning)

      if( deleted )
         this.coreService.presentToast('success', 'Le gain a bien été supprimé.')
      else
         this.coreService.presentToast('error', 'Erreur : Le gain n\'a pas pu être supprimé.')

      this.dismissModal()
   }

   /**
   * Presente delete alert choice. 
   * If user confirm, delete earning & dismiss modal
   */
   public presentDeleteAlert = async(): Promise<void> => 
   {
      const alert = await this.alertController.create({
         header: 'Attention !',
         message: '<strong>La suppression est irréversible</strong>. Êtes-vous sûr de vouloir supprimer ce gain définitivement ?',
         buttons: [
            {
               text: 'Annuler',
               role: 'cancel'
            }, {
               text: 'Supprimer',
               handler: () => this.deleteEarning()
            }
         ]
      })

      await alert.present()
   }

   /**
   * Dismiss modal
   */
   public dismissModal = (): void =>
   {
      this.modalController.dismiss()
   }

   /**
   * Save earning form 
   */
   public submit = async(): Promise<void> => 
   {
      Object.assign(this.earning, this.earningForm.value)

      const saved = await this.earningsService.save(this.earning)

      if( saved )
         this.coreService.presentToast('success', 'Le gain a bien été pris en compte.')
      else 
         this.coreService.presentToast('error', 'Erreur : Le gain n\'a pas pu être enregistré.')

      this.dismissModal()
   }

}
