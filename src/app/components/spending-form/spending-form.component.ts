import { Component, OnInit, Input } from '@angular/core'
import { FormGroup, Validators, FormBuilder } from '@angular/forms'

import { AlertController, ModalController } from '@ionic/angular'

import { SpendingsService, Spending } from './../../services/spendings/spendings.service'
import { CoreService } from './../../services/core/core.service'

@Component({
   selector    : 'common-spending-form',
   templateUrl : './spending-form.component.html'
})

export class SpendingFormComponent implements OnInit 
{
   @Input() spending: Spending

   public spendingForm: FormGroup

   constructor(
      private spendingsService: SpendingsService,
      private modalController: ModalController,
      private alertController: AlertController,
      private coreService: CoreService,
      private formBuilder: FormBuilder
   ){}

   ngOnInit(){
      this.spendingForm = this.getFormGroup()
   }
   
   /**
   * Create FormGroup for spending form
   * @return { FormGroup }
   */
   private getFormGroup = (): FormGroup => 
   {
      return this.formBuilder.group({
         title: [ 
            this.spending.title, 
            Validators.required 
         ],
         amount : [
            this.spending.amount, 
            [ 
               Validators.required, 
               Validators.min(0.01),
               Validators.pattern(/^[0-9]+(\.[0-9]{1,2})?$/)
            ]
         ],
         folder_id : [
            // Select value only accept string
            this.spending.folder_id != null ? this.spending.folder_id.toString() : null
         ]
      })
   }

   /**
   * Delete this spending
   */
   private deleteSpending = async(): Promise<void> =>
   {
      const deleted = await this.spendingsService.delete(this.spending)

      if( deleted )
         this.coreService.presentToast('success', 'La dépense a bien été supprimée.')
      else
         this.coreService.presentToast('error', 'Erreur : La dépense n\'a pas pu être supprimée.')

      this.dismissModal()
   }

   /**
   * Presente delete alert choice. 
   * If user confirm, delete spending & dismiss modal
   */
   public presentDeleteAlert = async(): Promise<void> => 
   {
      const alert = await this.alertController.create({
         header: 'Attention !',
         message: '<strong>La suppression est irréversible</strong>. Êtes-vous sûr de vouloir supprimer cette dépense définitivement ?',
         buttons: [
            {
               text: 'Annuler',
               role: 'cancel'
            }, {
               text: 'Supprimer',
               handler: () => this.deleteSpending()
            }
         ]
      })

      await alert.present()
   }

   /**
   * Dismiss modal
   */
   public dismissModal = (): void =>
   {
      this.modalController.dismiss()
   }

   /**
   * Save earning form 
   */
   public submit = async(): Promise<void> => 
   {
      Object.assign(this.spending, this.spendingForm.value)

      // Select value only accept string, so, if user select a folder parse string id to int id 
      this.spending.folder_id = this.spendingForm.value.folder_id != null ? parseInt(this.spendingForm.value.folder_id) : null

      const saved = await this.spendingsService.save(this.spending)

      if( saved )
         this.coreService.presentToast('success', 'La dépense a bien été prise en compte.')
      else 
         this.coreService.presentToast('error', 'Erreur : La dépense n\'a pas pu être prise en compte.')

      this.dismissModal()
   }

}
