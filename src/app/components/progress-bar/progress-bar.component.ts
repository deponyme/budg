import { Component, OnInit } from '@angular/core'

import { ProgressBarService, ProgressBarValues } from './../../services/progress-bar/progress-bar.service'
import { SpendingsService } from './../../services/spendings/spendings.service'
import { EarningsService } from './../../services/earnings/earnings.service'
import { LeviesService } from './../../services/levies/levies.service'
import { CoreService } from './../../services/core/core.service'

@Component({
   selector: 'common-progress-bar',
   templateUrl: './progress-bar.component.html'
})

export class ProgressBarComponent implements OnInit {

   public values: ProgressBarValues 
   public isInit: boolean

   constructor(
      private progressBarService : ProgressBarService,
      private spendingsService : SpendingsService,
      private earningsService : EarningsService,
      private leviesService : LeviesService,
      private coreService : CoreService
   ){}

   ngOnInit() 
   {
      this.setValues()

      this.coreService.currentDateChange.subscribe( (): void => 
      { 
         this.setValues()
      })

      this.spendingsService.spendingsChanged.subscribe( (): void => 
      { 
         this.setValues()
      })

      this.earningsService.earningsChanged.subscribe( (): void => 
      { 
         this.setValues()
      })

      this.leviesService.leviesChanged.subscribe( (): void => 
      { 
         this.setValues()
      })
   }

   /**
   * Set progress bar value
   */
   private setValues = async (): Promise<void> =>
   {
      this.isInit = false
      this.values = await this.progressBarService.getMonthProgressBarValuesForCoreCurrentDate()
      this.isInit = true
   }

   /**
   * @return {string} progress bar color
   */
   public getProgressBarColor = ():string => 
   {
      let color = 'success'

      if( this.values.progressPercentage > 0.6 )
         color = 'warning'
      
      if( this.values.progressPercentage > 0.8 ) 
         color = 'danger'

      return color
   } 
}
