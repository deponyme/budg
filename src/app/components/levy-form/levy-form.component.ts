import { Component, OnInit, Input } from '@angular/core'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { DatePipe } from '@angular/common'

import { ToastController, AlertController, ModalController } from '@ionic/angular'

import { LeviesService, Levy } from './../../services/levies/levies.service'
import { CoreService } from './../../services/core/core.service'

@Component({
   selector: 'app-levy-form',
   templateUrl: './levy-form.component.html'
})
export class LevyFormComponent implements OnInit {

   @Input() levy : Levy
   public levyForm : FormGroup

   constructor(
      private modalController : ModalController,
      private toastController: ToastController,
      private alertController: AlertController,
      private leviesService: LeviesService,
      private formBuilder: FormBuilder,
      private coreService: CoreService
   ){}

   ngOnInit() 
   {
      this.levyForm = this.getFormGroup()
   }

   /**
   * Create FormGroup for levy form
   * @return { FormGroup }
   */
   private getFormGroup = (): FormGroup =>
   {
      const datePipe : DatePipe = new DatePipe('fr-FR')

      let formGroup = this.formBuilder.group({
         title: [ 
            this.levy.title, 
            Validators.required 
         ],
         amount : [
            this.levy.amount, 
            [ 
               Validators.required, 
               Validators.min(0.01),
               Validators.pattern(/^[0-9]+(\.[0-9]{1,2})?$/)
            ]
         ],
         day : [
            this.levy.day != null ? this.levy.day.toString() : '1', 
            Validators.required
         ],
         start : [
            this.levy.start != null ? datePipe.transform(this.levy.start, 'yyyy-MM-dd') : datePipe.transform(this.coreService.getCurrentDate(), 'yyyy-MM-dd'),
            Validators.required
         ],
         end : [
            this.levy.end != null ? datePipe.transform(this.levy.end, 'yyyy-MM-dd') : '',
            Validators.required
         ],
         folder_id : [
            this.levy.folder_id != null ? this.levy.folder_id.toString() : null
         ]
      }, 
      { 
         validator: this.startMustBeLessThanEnd('start', 'end') 
      })

      return formGroup
   }

   /**
   * Custom formGroup Validator. Check if start date is less than end date
   */
   private startMustBeLessThanEnd = (start: string, end: string): {} =>
   {
      return (group: FormGroup): {[key: string]: any} => 
      {
         let f = group.controls[start];
         let t = group.controls[end];
         if (f.value > t.value) {
            return {
               dates: "Date start should be less than Date end"
            }
         }
         return {}
      }
   }

   /**
   * Dismiss modal
   */
   public dismissModal = (): void =>
   {
      this.modalController.dismiss()
   }

   /**
   * Delete this levy
   */
   private deleteLevy = async(): Promise<void> =>
   {
      const deleted = await this.leviesService.delete(this.levy)

      if( deleted )
         this.coreService.presentToast('success', 'Le prélèvement a bien été supprimé.')
      else
         this.coreService.presentToast('error', 'Erreur : Le prélèvement n\'a pas pu être supprimé.')

      this.dismissModal()
   }

   /**
   * Presente delete alert choice. 
   * If user confirm, delete levy & dismiss modal
   */
   public presentDeleteAlert = async(): Promise<void> => 
   {
      const alert = await this.alertController.create({
         header: 'Attention !',
         message: '<strong>La suppression est irréversible</strong>. Êtes-vous sûr de vouloir supprimer ce prélèvement définitivement ?',
         buttons: [
            {
               text: 'Annuler',
               role: 'cancel'
            }, {
               text: 'Supprimer',
               handler: () => this.deleteLevy()
            }
         ]
      })

      await alert.present()
   }

   /**
   * Save earning form 
   */
   public submit = async(): Promise<void> => 
   {
      const inputs = this.levyForm.value

      Object.assign(this.levy, this.levyForm.value)

      this.levy.folder_id = inputs.folder_id != null ? parseInt(inputs.folder_id) : null
      this.levy.day = parseInt(inputs.day)
      this.levy.start = new Date(inputs.start)
      this.levy.end = new Date(inputs.end)

      const saved = await this.leviesService.save(this.levy)

      if( saved )
         this.coreService.presentToast('success', 'Le groupe de dépense a bien été pris en compte.')
      else 
         this.coreService.presentToast('error', 'Erreur : Le prélèvement n\'a pas pu être pris en compte.')

      this.dismissModal()
   }

}
