import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ReactiveFormsModule } from '@angular/forms'

import { IonicModule } from '@ionic/angular'

import { PipesModule } from './../pipes/pipes.module'

import { MonthSelectorComponent } from './month-selector/month-selector.component'
import { SpendingFormComponent } from './spending-form/spending-form.component'
import { EarningFormComponent } from './earning-form/earning-form.component'
import { ProgressBarComponent } from './progress-bar/progress-bar.component'
import { LevyFormComponent } from './levy-form/levy-form.component'
import { FolderSelectComponent } from './folder-select/folder-select.component'

@NgModule({
   imports:[
      CommonModule,
      IonicModule,
      ReactiveFormsModule,
      PipesModule
   ],
   declarations: [
      MonthSelectorComponent,
      SpendingFormComponent,
      EarningFormComponent,
      ProgressBarComponent,
      FolderSelectComponent,
      LevyFormComponent
   ],
   exports: [
      MonthSelectorComponent,
      SpendingFormComponent,
      EarningFormComponent,
      ProgressBarComponent,
      FolderSelectComponent,
      LevyFormComponent
   ],
   entryComponents: [
      SpendingFormComponent,
      EarningFormComponent,
      FolderSelectComponent,
      LevyFormComponent
   ]
})
export class AppCommonModule {}