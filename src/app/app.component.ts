import { Component, OnInit } from '@angular/core'

import { UpdateService } from './services/update/update.service'

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})

export class AppComponent implements OnInit
{
  public appPages: AppPage[] = 
  [
    new AppPage('Connexion', '/user-login', 'person'),
    new AppPage('Synthèse', '/calendar', 'calendar'),
    new AppPage('Prélévements', '/levies', 'repeat'),
    new AppPage('Groupes de dépenses', '/folders', 'pricetags'),
    new AppPage('Statistiques', '/stats', 'stats'),
    new AppPage('Cloud', '/cloud', 'cloud')
  ]

  constructor(
    private updateService: UpdateService
  ){}

  ngOnInit()
  {
    this.updateService.runUpdateChecker() 
  }

}

export class AppPage 
{
  constructor(
    public title: string,
    public url: string,
    public icon: string
  ){}
  
}
