import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

const routes: Routes = [
  { path: '', redirectTo: 'calendar', pathMatch: 'full' },
  { path: 'calendar', loadChildren: './modules/calendar/calendar.module#CalendarPageModule' },
  { path: 'levies', loadChildren: './modules/levies/levies.module#LeviesPageModule' },
  { path: 'folders', loadChildren: './modules/folders/folders.module#FoldersPageModule' },
  { path: 'stats', loadChildren: './modules/stats/stats.module#StatsPageModule' },
  { path: 'cloud', loadChildren: './modules/cloud/cloud.module#CloudPageModule' },
  { path: 'user-login', loadChildren: './modules/user-login/user-login.module#UserLoginPageModule' }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {}
