import { Injectable, Output, EventEmitter } from '@angular/core'

import { CoreService } from '../core/core.service'

@Injectable({
   providedIn: 'root'
})

export class EarningsService
{
   @Output() earningsChanged: EventEmitter<void> = new EventEmitter()

   constructor(
      private coreService: CoreService
   ){}

   /**
   * Create/Update indexedDB earnings database
   * @param {IDBOpenDBRequest}
   * @return {Promise<boolean>}
   */
   private updateStore = async(store: IDBOpenDBRequest): Promise<boolean> =>
   {
      let updated: boolean = false

      await new Promise( resolve => 
      {
         store
            .result
            .createObjectStore("earnings", { keyPath: "id" })
            .createIndex("by_dates", "date");

         store.onsuccess = (): void => 
         {
            updated = true
            resolve()
         }
         
         store.onerror = (error: any): void => 
         {
            console.log('Can\'t create/update object store for earnings', error) 
            resolve()
         }
      })

      return updated
   }

   /**
   * Open indexedDB earnings database
   * @return {Promise<IDBOpenDBRequest>}
   */
   private openStore = async(): Promise<IDBOpenDBRequest> =>
   {
      let store: IDBOpenDBRequest = null

      await new Promise( resolve => 
      {
         const database = indexedDB.open("earnings")

         database.onupgradeneeded = async(): Promise<void> => 
         {
            await this.updateStore(database)
            store = database
            resolve()
         }

         database.onsuccess = (): void => {
            store = database
            resolve()
         }
         
         database.onerror = (error: any): void => 
         {
            console.log('Can\'t open object store for earnings', error) 
            resolve()
         } 
      })

      return store
   }

   /**
   * Add earning to indexedDB earnings database
   * @param {IDBObjectStore} - indexedDB earnings database
   * @param {Earning}
   * @return {Promise<number>} - Saved earning identifier
   */
   private addToStore = async(store: IDBObjectStore,  earning: Earning): Promise<boolean> =>
   {
      let success: boolean = false

      await new Promise( resolve => 
      {
         earning.id = this.coreService.generateNewId()
         const request: IDBRequest = store.add(earning)

         request.onsuccess = () => 
         {
            success = true
            resolve()
         }
         
         request.onerror = (error: any): void => 
         {
            console.log('Can\'t add object to earnings store',  earning, error) 
            resolve()
         }
      })

      if( success )
         this.earningsChanged.emit()

      return success
   }

   /**
   * Put earning to indexedDB earnings database
   * @param {IDBObjectStore} - indexedDB earnings database
   * @param {Earning}
   * @return {Promise<number>} - Saved earning identifier
   */
   private putToStore = async(store: IDBObjectStore,  earning: Earning): Promise<boolean> =>
   {
      let success: boolean = false

      await new Promise( resolve => 
      {
         const request: IDBRequest = store.put(earning)

         request.onsuccess = () => 
         {
            success = true
            resolve()
         }
         
         request.onerror = (error: any): void => 
         {
            console.log('Can\'t put object to earnings store',  earning, error) 
            resolve()
         }
      })

      if( success )
         this.earningsChanged.emit()

      return success
   }

   /**
   * Save earning into indexedDB earnings database
   * @param {Earning}
   * @return {Promise<boolean>}
   */
   public save = async(earning:Earning): Promise<boolean> =>
   {
      const request:IDBOpenDBRequest = await this.openStore()
      
      if( request == null )
         return false

      const store:IDBObjectStore = request.result
         .transaction("earnings", "readwrite")
         .objectStore("earnings")

      if( earning.id == null )
         return await this.addToStore(store, earning)
      else
         return await this.putToStore(store, earning)
   }

   /**
   * Delete earning from indexedDB earnings database
   * @param {Earning}
   * @return {Promise<boolean>}
   */
   public delete = async(earning: Earning): Promise<boolean> => 
   {
      let success: boolean = false

      await new Promise( async resolve => 
      {
         const store:IDBOpenDBRequest = await this.openStore()
         
         if( store == null ) 
            resolve()

         const request:IDBRequest = store.result
            .transaction("earnings", "readwrite")
            .objectStore("earnings")
            .delete(earning.id)

         request.onsuccess = (): void => 
         {
           success = true
           resolve() 
         }

         request.onerror = (error: any): void => 
         {
            console.log('Can\'t delete earning', earning, error)
            resolve()
         }
      })

      if( success )
         this.earningsChanged.emit()

      return success
   }

   /**
   * Get earnings for a day
   * @param {Date}
   * @return {Earning[]}
   */
   public getForDay = async(date: Date): Promise<Earning[]> =>
   {
      const from : Date = new Date(date)
      const to : Date = new Date(date)

      return await this.getBetween(from, to)
   }

   /**
   * Get earnings for a month
   * @param {number} month -  number from 0 to 11
   * @param {number} year  - month's year
   * @return {Promise<Earning[]>}  
   */
   public getForMonth = async(month:number, year:number):Promise<Earning[]> => 
   {
      const from : Date = new Date(year, month, 1)
      const to : Date = new Date(year, month+1, 0)

      return await this.getBetween(from, to)
   }

   /**
   * Get earnings from "date" to "date"
   * @param {Date} from - From date
   * @param {Date} to - To date
   * @return {Promise<Earning[]>} - Earnings array  
   */
   public getBetween = async(from: Date, to: Date): Promise<Earning[]> =>
   {
      let earnings : Earning[] = []

      await new Promise( async resolve => 
      {
         const store: IDBOpenDBRequest = await this.openStore()
         const range: IDBKeyRange = IDBKeyRange.bound(from, to)
            
         if( store == null ) 
            resolve()

         const index: IDBIndex = store.result
            .transaction("earnings", "readonly")
            .objectStore("earnings")
            .index("by_dates")

         index.openCursor(range).onsuccess = (e: any): void => 
         {
            const cursor = e.target.result
                  
            if(cursor) 
            {
               earnings.push(cursor.value)
               cursor.continue()
            }
            else 
               resolve()
         }

         index.openCursor(range).onerror = (error:any): void => 
         {
            console.log('Can\'t get earnings beetween date (openCursor error)', from, to, error)
            resolve()
         }
      })

      return earnings 
   }

   /**
   * Get earnings amount sum for a month
   * @param {number} month -  number from 0 to 11
   * @param {number} year  - month's year
   * @return {Promise<number>} - earnings sum
   */
   public getEarningsSumForMonth = async(month: number, year: number): Promise<number> =>
   {
      let sum: number = 0

      const earnings = await this.getForMonth(month, year)

      if( earnings.length == 1 )
         sum = earnings[0].amount

      else if( earnings.length > 1 )
         sum = earnings
            .map( (earning: Earning): number => {
               return earning.amount
            })
            .reduce( (a: number,b: number): number => {
               return a+b
            })

      return sum
   }

}

export class Earning
{
   constructor(
      public title  : string = null,
      public amount : number = null,
      public date   : Date = null,
      public id     : number = null
   ){}
   
}
