import { TestBed } from '@angular/core/testing';

import { EarningsService } from './earnings.service';

describe('EarningsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EarningsService = TestBed.get(EarningsService);
    expect(service).toBeTruthy();
  });
});
