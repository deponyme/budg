import { Injectable, Output, EventEmitter } from '@angular/core'
import { SwUpdate } from '@angular/service-worker'

import { ToastController, AlertController } from '@ionic/angular'

@Injectable({
   providedIn: 'root'
})

export class CoreService 
{
   private currentMonth: number = new Date().getMonth()
   private currentYear: number  = new Date().getFullYear()

   @Output() currentDateChange: EventEmitter<Date> = new EventEmitter()

   constructor(
      private toastController: ToastController
   ){}

   /**
   * Generate an entity ID
   * @return {number}
   */
   public generateNewId = ():number => 
   {
      const tsInMs = Date.now()

      return Math.floor(tsInMs/1000)
   }

   /**
   * Presente toast message
   * @param {string} color - toast color (success,danger,primary ...) 
   * @param {message} message - message diplayed by toast
   */
   public presentToast = async (color: string, message: string, duration: number = 2000): Promise<void> => 
   {
      const toast = await this.toastController.create({
         message,
         duration,
         showCloseButton: false,
         position: 'bottom',
         closeButtonText: 'Fermer',
         color,
      })
      
      await toast.present()
   }

   /**
   * Set current date used by all app's modules
   * @param {Date}
   */
   public setCurrentDate = (date: Date): void =>
   {
      this.currentMonth = date.getMonth()
      this.currentYear = date.getFullYear()

      this.currentDateChange.emit(this.getCurrentDate())
   }

   /**
   * Get current month used by app ( 0 = January / 11 = December )
   * @return {number}
   */
   public getCurrentMonth = (): number =>
   {
      return this.currentMonth
   }

   /**
   * Get current year used by app
   * @return {number}
   */
   public getCurrentYear = (): number => 
   {
      return this.currentYear
   }

   /**
   * Get current date used by app
   * @return {Date}
   */
   public getCurrentDate = (): Date => 
   {
      return new Date(this.currentYear, this.currentMonth, 1)
   }
}
