import { Injectable } from '@angular/core'
import { CoreService } from '../core/core.service'

@Injectable({
   providedIn: 'root'
})

export class CalendarService {
   
   /**
   * @property {string[]} - Letters for calendar columns
   */
   public headerLetters: string[] = ['L', 'M', 'M', 'J', 'V', 'S', 'D']

   constructor(
      private coreService: CoreService
   ) {}

   /**
   * Generate Array of CalendarCell Array
   * @return Array<CalendarCell[]>
   */
   public getCalendarArray = (): Array<CalendarCell[]> =>
   {
      const cells: CalendarCell[] = this.generateCells()

      return this.setCellsIntoRows(cells)
   }
   
   /**
   * Splite calendar cells into rows ( for week ) : 1 row = 7 cells 
   * @param {CalendarCell[]}
   * @return {Array<CalendarCell[]>}
   */
   private setCellsIntoRows = (cells: Array<CalendarCell>): Array<CalendarCell[]> => 
   {
      let rows = []
      const nbOfRows = Math.ceil( cells.length / 7 )

      for (let i = 0; i < nbOfRows; i++) 
      {
         rows[i] = cells.splice(0, 7)
      }

      const lastRow = rows[rows.length-1]
      const paddingCells = 7 - lastRow.length

      for (let n = 0; n < paddingCells; n++) 
      {
         lastRow.push(new CalendarCell(null))
      }

      return rows
   }

   /**
   * Generate calendar cells for current month
   * @return {CalendarCell[]}
   */
   private generateCells = (): CalendarCell[] =>
   {
      let cells: CalendarCell[] = []
      const month: number = this.coreService.getCurrentMonth()
      const year: number  = this.coreService.getCurrentYear()

      // Add to first week previous month days ( padding ... )
      const prevMonthDays: number = new Date(year, month, 0).getDay()

      for (let i = 1; i <= prevMonthDays; i++) 
      {
         cells.push(new CalendarCell(null))
      }

      // Add month days
      const calendarMonthDays: number = new Date(year, month+1, 0).getDate()
      for (let i = 1; i <= calendarMonthDays; i++) 
      {
         const cellDate: Date = new Date(year, month, i)
         
         cells.push(new CalendarCell(cellDate))
      }

      return cells
   }

}

export class CalendarCell 
{
   constructor(
      public date : Date
   ){}
   
}
