import { Injectable } from '@angular/core'

import { SpendingsService, Spending } from '../spendings/spendings.service'
import { EarningsService, Earning } from '../earnings/earnings.service'
import { FoldersService, Folder } from '../folders/folders.service'
import { LeviesService, Levy } from '../levies/levies.service'
import { CoreService } from '../core/core.service'

@Injectable({
   providedIn: 'root'
})

export class StatsService 
{   
   constructor(
      private spendingsService: SpendingsService,
      private earningsService: EarningsService,
      private foldersService: FoldersService,
      private leviesService: LeviesService,
      private coreService: CoreService
   ){}

   /**
   * Get sum for spendings / earnings / levies and month spending / earnings / levies for core current month
   * @return {Promise<Stats>} - Stats object
   */
   public getStats = async():Promise<Stats> => 
   {
      const coreCurrentMonth: number = this.coreService.getCurrentMonth()
      const coreCurrentYear: number = this.coreService.getCurrentYear()

      const earnings: Earning[] = await this.earningsService.getForMonth(coreCurrentMonth, coreCurrentYear)
      const spendings: Spending[] = await this.spendingsService.getForMonth(coreCurrentMonth, coreCurrentYear)
      const levies: Levy[] = await this.leviesService.getForMonth(coreCurrentMonth, coreCurrentYear) 
      const folders: Folder[]  = await this.foldersService.getAll()

      const earningsSum: number = await this.earningsService.getEarningsSumForMonth(coreCurrentMonth, coreCurrentYear)
      const spendingsSum: number = await this.spendingsService.getSpendingsSumForMonth(coreCurrentMonth, coreCurrentYear)
      const leviesSum: number = await this.leviesService.getLeviesSumForMonth(coreCurrentMonth, coreCurrentYear)

      const foldersStats = folders
         .map( (folder:Folder):FoldersStats => 
         {
            return new FoldersStats( 
               folder, 
               this.getLeviesSumForFolder(folder, levies),
               this.getSpendingsSumForFolder(folder, spendings)
            )
         })
         .filter( (foldersStats:FoldersStats):boolean => 
         {
            return foldersStats.leviesAmountSum > 0 || foldersStats.spendingsAmountSum > 0
         })
         .sort( (a:FoldersStats,b:FoldersStats):number =>
         {
            const aSum: number = a.leviesAmountSum + a.spendingsAmountSum
            const bSum: number = b.leviesAmountSum + b.spendingsAmountSum
            
            if( aSum < bSum) 
               return 1

            if(aSum > bSum) 
               return -1

            return 0
         })

      
      const unclassifiedFolder: Folder = this.foldersService.getUnclassifiedFolder()
      let unclassifiedStats: FoldersStats = new FoldersStats( 
         unclassifiedFolder,
         this.getLeviesSumForFolder(unclassifiedFolder, levies),
         this.getSpendingsSumForFolder(unclassifiedFolder, spendings)
      )

      if( unclassifiedStats.leviesAmountSum == 0 && unclassifiedStats.spendingsAmountSum == 0 )
         unclassifiedStats = null

      return new Stats(earningsSum, spendingsSum, leviesSum, earnings, spendings, levies, foldersStats, unclassifiedStats)
   }


   /**
   * Get amount sum from stats spendings for a folder
   * @param {Folder}
   * @return {number} - amount sum 
   */
   private getSpendingsSumForFolder = (folder:Folder, spendings:Spending[]):number => 
   {
      const spendingsAmount: number[] = spendings
         .filter( (spending:Spending):boolean => {
            return spending.folder_id == folder.id
         })
         .map( (spending:Spending):number => {
            return spending.amount
         })

      if( spendingsAmount.length > 0 )
         return spendingsAmount.reduce( (a:number,b:number):number => {
            return a+b
         })

      return 0
   }

   /**
   * Get amount sum from stats levies for a folder
   * @param {Folder}
   * @return {number} - amount sum 
   */
   private getLeviesSumForFolder = (folder:Folder, levies:Levy[]):number => 
   {
      const leviesAmount: number[] = levies
         .filter( (levy:Levy):boolean => {
            return levy.folder_id == folder.id
         })
         .map( (levy:Levy):number => {
            return levy.amount
         })

      if( leviesAmount.length > 0 )
         return leviesAmount.reduce( (a:number,b:number):number => {
            return a+b
         })

      return 0
   }
}

export class Stats 
{
   constructor(
      public earningsSum  : number = 0,
      public spendingsSum : number = 0,
      public leviesSum    : number = 0,
      public earnings     : Earning[] = [],
      public spendings    : Spending[] = [],
      public levies       : Levy[] = [],
      public folders      : FoldersStats[] = [],
      public unclassified : FoldersStats = null
   ){}

}

export class FoldersStats
{
   constructor(
      public folder             : Folder = null,
      public leviesAmountSum    : number = 0,
      public spendingsAmountSum : number = 0
   ){}
   
}
