import { Injectable } from '@angular/core'

@Injectable({
  providedIn: 'root'
})

export class UserService 
{

  constructor() { }

  /**
  * If user is logged return true, else return false
  * @return {boolean}
  */
  public userIsLogged = (): boolean => 
  {
    return false
  }

}

export class User
{
   constructor(
      public id       : number = null,
      public email    : string = null,
      public password : string = null
   ){}

}
