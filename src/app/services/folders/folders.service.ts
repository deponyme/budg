import { Injectable, Output, EventEmitter } from '@angular/core'

import { CoreService } from '../core/core.service'

@Injectable({
   providedIn: 'root'
})

export class FoldersService 
{
   @Output() foldersChanged: EventEmitter<void> = new EventEmitter()

   constructor(
      private coreService: CoreService
   ){}

   /**
   * Create/Update indexedDB folders database
   * @param {IDBOpenDBRequest}
   * @return {Promise<boolean>}
   */
   private updateStore = async(store: IDBOpenDBRequest): Promise<boolean> =>
   {
      let updated: boolean = false

      await new Promise( resolve => 
      {
         store
            .result
            .createObjectStore("folders", { keyPath: "id" })
            .createIndex("by_title", "title")

         store.onsuccess = (): void => 
         {
            updated = true
            resolve()
         }
         
         store.onerror = (error: any): void => 
         {
            console.log('Can\'t create/update object store for folders', error) 
            resolve()
         }
      })

      return updated
   }

   /**
   * Open indexedDB folders database
   * @return {Promise<IDBOpenDBRequest>}
   */
   private openStore = async(): Promise<IDBOpenDBRequest> =>
   {
      let store: IDBOpenDBRequest = null

      await new Promise( resolve => 
      {
         const database = indexedDB.open("folders")

         database.onupgradeneeded = async(): Promise<void> => 
         {
            await this.updateStore(database)
            store = database
            resolve()
         }

         database.onsuccess = (): void => {
            store = database
            resolve()
         }
         
         database.onerror = (error: any): void => 
         {
            console.log('Can\'t open object store for folders', error) 
            resolve()
         } 
      })

      return store
   }

   /**
   * Add folder to indexedDB folders database
   * @param {IDBObjectStore} - indexedDB folders database
   * @param {Folder}
   * @return {Promise<number>} - Saved folder identifier
   */
   private addToStore = async(store: IDBObjectStore,  folder: Folder): Promise<boolean> =>
   {
      let success: boolean = false

      await new Promise( resolve => 
      {
         folder.id = this.coreService.generateNewId()
         const request: IDBRequest = store.add(folder)

         request.onsuccess = async(event: any): Promise<void> => 
         {
            success = true
            resolve()
         }
         
         request.onerror = (error: any): void => 
         {
            console.log('Can\'t add object to folders store', folder, error) 
            resolve()
         }
      })

      if( success )
         this.foldersChanged.emit()

      return success
   }

   /**
   * Put folder to indexedDB folders database
   * @param {IDBObjectStore} - indexedDB folders database
   * @param {Folder}
   * @return {Promise<number>} - Saved folder identifier
   */
   private putToStore = async(store: IDBObjectStore,  folder: Folder): Promise<boolean> =>
   {
      let success: boolean = false

      await new Promise( resolve => 
      {
         const request: IDBRequest = store.put(folder)

         request.onsuccess = () => 
         {
            success = true
            resolve()
         }
         
         request.onerror = (error: any): void => 
         {
            console.log('Can\'t put object to folders store',  folder, error) 
            resolve()
         }
      })

      if( success )
         this.foldersChanged.emit()

      return success
   }

   /**
   * Save folder into indexedDB folders database
   * @param {Folder}
   * @return {Promise<boolean>}
   */
   public save = async(folder:Folder): Promise<boolean> =>
   {
      const request:IDBOpenDBRequest = await this.openStore()
      
      if( request == null )
         return false

      const store:IDBObjectStore = request.result
         .transaction("folders", "readwrite")
         .objectStore("folders")

      if( folder.id == 0 )
         return await this.addToStore(store, folder)
      else
         return await this.putToStore(store, folder)
   }

   /**
   * Get 'virtual' unclassified folder 
   */
   public getUnclassifiedFolder = (): Folder =>
   {
      return new Folder('Non classés')
   }

   /**
   * Get folders
   * @param {boolean} returnDeletedFolders (Default:false) - false: only folder without deleted flag | true: only folder with deleted flag 
   * @return {Promise<Folder[]>}
   */
   public get = async(returnDeletedFolders: boolean = false):Promise<Folder[]> =>
   {
      let folders: Folder[] = []

      await new Promise( async resolve => 
      {
         const store: IDBOpenDBRequest = await this.openStore()

         const index: IDBIndex = store.result
            .transaction("folders", "readonly")
            .objectStore("folders")
            .index("by_title")
              
         index.getAll().onsuccess = (e: any): void => 
         {
            folders = e.target.result.filter( (folder:Folder):boolean => 
            {
               if( returnDeletedFolders )
                  return folder.deleted
                        
               return !folder.deleted
            }) 

            resolve() 
         }

         index.getAll().onerror = (error: any):void => 
         {
            console.log('Can\'t get folders on folders database', error)
            resolve()
         }
      })

      return folders
   }

   /**
   * Get ALL folders (with and without deleted flag)
   * @return {Promise<Folder[]>} - Folder array
   */
   public getAll = async(): Promise<Folder[]> => 
   {
      return []
         .concat(await this.get())
         .concat(await this.get(true))
   }

   /**
   * Delete folder
   * @param {Folder}
   * @return {Promise<boolean>}
   */
   public delete = async(folder: Folder): Promise<boolean> => 
   {
      folder.deleted = true

      const savedWithDeletedFlag = await this.save(folder)

      if( savedWithDeletedFlag )
         return true

      return false
   }

}

export class Folder 
{
   constructor(
      public title   : string = null,
      public deleted : boolean = false,
      public id      : number = 0
   ){}
   
}
