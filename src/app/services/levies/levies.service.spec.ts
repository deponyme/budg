import { TestBed } from '@angular/core/testing';

import { LeviesService } from './levies.service';

describe('LeviesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LeviesService = TestBed.get(LeviesService);
    expect(service).toBeTruthy();
  });
});
