import { Injectable, Output, EventEmitter } from '@angular/core'

import { CoreService } from '../core/core.service'

@Injectable({
   providedIn: 'root'
})

export class LeviesService 
{
   @Output() leviesChanged: EventEmitter<any> = new EventEmitter()

   constructor(
      private coreService: CoreService
   ){}

   /**
   * Create/Update indexedDB levies database
   * @param {IDBOpenDBRequest}
   * @return {Promise<boolean>}
   */
   private updateStore = async(store: IDBOpenDBRequest): Promise<boolean> =>
   {
      let updated: boolean = false

      await new Promise( resolve => 
      {
         store
            .result
            .createObjectStore("levies", { keyPath: "id" })
            .createIndex("by_start", "start")

         store.onsuccess = (): void => 
         {
            updated = true
            resolve()
         }
         
         store.onerror = (error: any): void => 
         {
            console.log('Can\'t create/update object store for earnings', error) 
            resolve()
         }
      })

      return updated
   }

   /**
   * Open indexedDB folders database
   * @return {Promise<IDBOpenDBRequest>}
   */
   private openStore = async(): Promise<IDBOpenDBRequest> =>
   {
      let store: IDBOpenDBRequest = null

      await new Promise( resolve => 
      {
         const database = indexedDB.open("levies")

         database.onupgradeneeded = async(): Promise<void> => 
         {
            await this.updateStore(database)
            store = database
            resolve()
         }

         database.onsuccess = (): void => {
            store = database
            resolve()
         }
         
         database.onerror = (error: any): void => 
         {
            console.log('Can\'t open object store for levies', error) 
            resolve()
         } 
      })

      return store
   }

   /**
   * Add levy to indexedDB levies database
   * @param {IDBObjectStore} - indexedDB levies database
   * @param {Levy}
   * @return {Promise<number>} - Saved levy identifier
   */
   private addToStore = async(store: IDBObjectStore,  levy: Levy): Promise<boolean> =>
   {
      let success: boolean = false

      await new Promise( resolve => 
      {
         levy.id = this.coreService.generateNewId()
         const request: IDBRequest = store.add(levy)

         request.onsuccess = async(event: any): Promise<void> => 
         {
            
            success = true
            resolve()
         }
         
         request.onerror = (error: any): void => 
         {
            console.log('Can\'t add object to levies store', levy, error) 
            resolve()
         }
      })

      if( success )
         this.leviesChanged.emit()

      return success
   }

   /**
   * Put levy to indexedDB levies database
   * @param {IDBObjectStore} - indexedDB levies database
   * @param {Levy}
   * @return {Promise<number>} - Saved levy identifier
   */
   private putToStore = async(store: IDBObjectStore,  levy: Levy): Promise<boolean> =>
   {
      let success: boolean = false

      await new Promise( resolve => 
      {
         const request: IDBRequest = store.put(levy)

         request.onsuccess = () => 
         {
            success = true
            resolve()
         }
         
         request.onerror = (error: any): void => 
         {
            console.log('Can\'t put object to levies store',  levy, error) 
            resolve()
         }
      })

      if( success )
         this.leviesChanged.emit()

      return success
   }

   /**
   * Save levy into indexedDB levies database
   * @param {Levy}
   * @return {Promise<boolean>}
   */
   public save = async(levy: Levy): Promise<boolean> =>
   {
      const request:IDBOpenDBRequest = await this.openStore()
      
      if( request == null )
         return false

      const store:IDBObjectStore = request.result
         .transaction("levies", "readwrite")
         .objectStore("levies")

      if( levy.id == null )
         return await this.addToStore(store, levy)
      else
         return await this.putToStore(store, levy)
   }

   /**
   * Get levies for a month
   * @param {number} month -  number from 0 to 11
   * @param {number} year  - month's year
   * @return {Levy[]} - Levy array  
   */
   public getForMonth = async(month: number, year: number): Promise<Levy[]> => 
   {
      let levies: Levy[] = []

      await new Promise( async resolve => 
      {
         const store:IDBOpenDBRequest = await this.openStore()

         if( store == null ) 
            resolve()

         const index: IDBIndex = store.result
            .transaction("levies", "readonly")
            .objectStore("levies")
            .index("by_start")

         index.getAll().onsuccess = (e: any): void => 
         {
            const results: Levy[] = e.target.result 

            const lastMonthDay: Date = new Date(year, month+1, 0)
            const firstMonthDay: Date = new Date(year, month, 1)

            if( results.length > 0 )
               levies = results
                  .filter( (levy:Levy) => {
                     if( levy.end.getTime() >= firstMonthDay.getTime() )
                        return levy
                  })
                  .filter( (levy:Levy) => {
                     if( lastMonthDay.getTime() >= levy.start.getTime()  )
                        return levy
                  })

            resolve()           
         }

         index.getAll().onerror = (error:any):void => 
         {
            console.log('Can\'t get levies for a month', error)
            resolve() 
         }
      })

      return levies
   }

   /**
   * Get levies amount sum for a month
   * @param {number} month -  number from 0 to 11
   * @param {number} year  - month's year
   * @return {Promise<number>} - levies sum
   */
   public getLeviesSumForMonth = async(month: number, year: number): Promise<number> =>
   {
      let sum: number = 0

      const levies = await this.getForMonth(month, year)

      if( levies.length == 1 )
         sum = levies[0].amount

      else if( levies.length > 1 )
         sum = levies
            .map( (levy: Levy): number => {
               return levy.amount
            })
            .reduce( (a: number,b: number): number => {
               return a+b
            })

      return sum
   }

   /**
   * Get levies for a day
   * @param {Date} - JS date object of the day
   * @return {Promise<Levy[]>} - Levies array  
   */
   public getForDay = async(date: Date): Promise<Levy[]> =>
   {
      const month : number = date.getMonth()
      const year : number = date.getFullYear()

      const levies: Levy[] = await this.getForMonth(month, year)

      return await levies.filter( (levy: Levy): boolean => 
      {
         return levy.day == date.getDate()
      })
   }

   /**
   * Delete levy from indexedDB levies database
   * @param {Levy}
   * @return {Promise<boolean>}
   */
   public delete = async(levy: Levy): Promise<boolean> => 
   {
      let success: boolean = false

      await new Promise( async resolve => 
      {
         const store:IDBOpenDBRequest = await this.openStore()
         
         if( store == null ) 
            resolve()

         const request:IDBRequest = store.result
            .transaction("levies", "readwrite")
            .objectStore("levies")
            .delete(levy.id)

         request.onsuccess = (): void => 
         {
            success = true
            resolve()
         }

         request.onerror = (error: any): void => 
         {
            console.log('Can\'t delete levy', levy, error)
            resolve()
         }
      })

      if( success )
         this.leviesChanged.emit()

      return success
   }

}

export class Levy
{
   constructor(
      public title     : string = null,
      public amount    : number = null,
      public day       : number = null,
      public start     : Date = null,
      public end       : Date = null,
      public folder_id : number = 0,
      public id        : number = null
   ){}

}