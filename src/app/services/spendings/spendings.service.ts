import { Injectable, Output, EventEmitter } from '@angular/core'

import { CoreService } from '../core/core.service'

@Injectable({
   providedIn: 'root'
})

export class SpendingsService
{
   @Output() spendingsChanged: EventEmitter<void> = new EventEmitter()

   constructor(
      private coreService: CoreService
   ){}

   /**
   * Create/Update indexedDB spendings database
   * @param {IDBOpenDBRequest}
   * @return {Promise<boolean>}
   */
   private updateStore = async(store: IDBOpenDBRequest): Promise<boolean> =>
   {
      let updated: boolean = false

      await new Promise( resolve => 
      {
         store
            .result
            .createObjectStore("spendings", { keyPath: "id" })
            .createIndex("by_dates", "date");

         store.onsuccess = (): void => 
         {
            updated = true
            resolve()
         }
         
         store.onerror = (error: any): void => 
         {
            console.log('Can\'t create/update object store for spendings', error) 
            resolve()
         }
      })

      return updated
   }

   /**
   * Open indexedDB spendings database
   * @return {Promise<IDBOpenDBRequest>}
   */
   private openStore = async(): Promise<IDBOpenDBRequest> =>
   {
      let store: IDBOpenDBRequest = null

      await new Promise( resolve => 
      {
         const database = indexedDB.open("spendings")

         database.onupgradeneeded = async(): Promise<void> => 
         {
            await this.updateStore(database)
            store = database
            resolve()
         }

         database.onsuccess = (): void => {
            store = database
            resolve()
         }
         
         database.onerror = (error: any): void => 
         {
            console.log('Can\'t open object store for spendings', error) 
            resolve()
         } 
      })

      return store
   }

   /**
   * Add spending to indexedDB spendings database
   * @param {IDBObjectStore} - indexedDB spendings database
   * @param {Spending}
   * @return {Promise<number>} - Saved spending identifier
   */
   private addToStore = async(store: IDBObjectStore,  spending: Spending): Promise<boolean> =>
   {
      let success: boolean = false

      await new Promise( resolve => 
      {
         spending.id = this.coreService.generateNewId()
         const request: IDBRequest = store.add(spending)

         request.onsuccess = async(event: any): Promise<void> => 
         {
            success = true
            resolve()
         }
         
         request.onerror = (error: any): void => 
         {
            console.log('Can\'t add object to spendings store', spending, error) 
            resolve()
         }
      })

      if( success )
         this.spendingsChanged.emit()

      return success
   }

   /**
   * Put spending to indexedDB spendings database
   * @param {IDBObjectStore} - indexedDB spendings database
   * @param {Spending}
   * @return {Promise<number>} - Saved spending identifier
   */
   private putToStore = async(store: IDBObjectStore,  spending: Spending): Promise<boolean> =>
   {
      let success: boolean = false

      await new Promise( resolve => 
      {
         const request: IDBRequest = store.put(spending)

         request.onsuccess = () => 
         {
            success = true
            resolve()
         }
         
         request.onerror = (error: any): void => 
         {
            console.log('Can\'t put object to spendings store',  spending, error) 
            resolve()
         }
      })

      if( success )
         this.spendingsChanged.emit()

      return success
   }

   /**
   * Save spending into indexedDB spendings database
   * @param {Spending}
   * @return {Promise<boolean>}
   */
   public save = async(spending:Spending): Promise<boolean> =>
   {
      const request:IDBOpenDBRequest = await this.openStore()
      
      if( request == null )
         return false

      const store:IDBObjectStore = request.result
         .transaction("spendings", "readwrite")
         .objectStore("spendings")

      if( spending.id == null )
         return await this.addToStore(store, spending)
      else
         return await this.putToStore(store, spending)
   }

   /**
   * Delete spending from indexedDB spendings database
   * @param {Spending}
   * @return {Promise<boolean>}
   */
   public delete = async(spending: Spending): Promise<boolean> => 
   {
      let success: boolean = false

      await new Promise( async resolve => 
      {
         const store:IDBOpenDBRequest = await this.openStore()
         
         if( store == null ) 
            resolve()

         const request:IDBRequest = store.result
            .transaction("spendings", "readwrite")
            .objectStore("spendings")
            .delete(spending.id)

         request.onsuccess = (): void => 
         {
           success = true
           resolve() 
         }

         request.onerror = (error: any): void => 
         {
            console.log('Can\'t delete spending', spending, error)
            resolve()
         }
      })

      if( success )
         this.spendingsChanged.emit()

      return success
   }

   /**
   * Get spendings for a day
   * @param {Date}
   * @return {Spending[]}
   */
   public getForDay = async(date: Date): Promise<Spending[]> =>
   {
      const from : Date = new Date(date)
      const to : Date = new Date(date)

      return await this.getBetween(from, to)
   }

   /**
   * Get spendings for a month
   * @param {number} month -  number from 0 to 11
   * @param {number} year  - month's year
   * @return {Promise<Spending[]>}  
   */
   public getForMonth = async(month:number, year:number):Promise<Spending[]> => 
   {
      const from : Date = new Date(year, month, 1)
      const to : Date = new Date(year, month+1, 0)

      return await this.getBetween(from, to)
   }

   /**
   * Get spendings from "date" to "date"
   * @param {Date} from - From date
   * @param {Date} to - To date
   * @return {Promise<Spending[]>} - Spendings array  
   */
   public getBetween = async(from: Date, to: Date): Promise<Spending[]> =>
   {
      let spendings : Spending[] = []

      await new Promise( async resolve => 
      {
         const store: IDBOpenDBRequest = await this.openStore()
         const range: IDBKeyRange = IDBKeyRange.bound(from, to)
            
         if( store == null ) 
            resolve()

         const index: IDBIndex = store.result
            .transaction("spendings", "readonly")
            .objectStore("spendings")
            .index("by_dates")

         index.openCursor(range).onsuccess = (e: any): void => 
         {
            const cursor = e.target.result
                  
            if(cursor) 
            {
               spendings.push(cursor.value)
               cursor.continue()
            }
            else
               resolve()
         }

         index.openCursor(range).onerror = (error:any): void => 
         {
            console.log('Can\'t get spendings beetween date (openCursor error)', from, to, error)
            resolve()
         }
      })

      return spendings
   }

   /**
   * Get spendings amount sum for a month
   * @param {number} month -  number from 0 to 11
   * @param {number} year  - month's year
   * @return {Promise<number>} - spendings sum
   */
   public getSpendingsSumForMonth = async(month: number, year: number): Promise<number> =>
   {
      let sum: number = 0

      const spendings = await this.getForMonth(month, year)

      if( spendings.length == 1 )
         sum = spendings[0].amount

      else if( spendings.length > 1 )
         sum = spendings
            .map( (spending: Spending): number => {
               return spending.amount
            })
            .reduce( (a: number,b: number): number => {
               return a+b
            })

      return sum
   }

}

export class Spending
{
   constructor(
      public title     : string = null,
      public amount    : number = null,
      public date      : Date = null,
      public folder_id : number = 0,
      public id        : number = null
   ){}
   
}
