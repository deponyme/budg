import { Injectable } from '@angular/core'
import { SwUpdate } from '@angular/service-worker'

import { AlertController, LoadingController } from '@ionic/angular'

@Injectable({
  providedIn: 'root'
})
export class UpdateService {

  constructor(
    private loadingController: LoadingController,
    private alertController: AlertController,
    private swUpdate: SwUpdate
  ){}

  /**
  * Check for update on run, and every hours
  */
  public runUpdateChecker = (): void => 
  {
    this.swUpdate.available.subscribe( () => 
    {
      this.presentUpdateAlert()
    })

    this.checkForUpdate()

    // setTimeout( () => {
    //   this.checkForUpdate()
    // }, 1*3.6e+6 )
  }

  /**
  * Check if update is available
  */
  public checkForUpdate = (): void =>
  {
    if (this.swUpdate.isEnabled) 
    {
      this.swUpdate.checkForUpdate()
    }
  }

  /**
  * Update pwa to latest available
  */
  private updateToLatest = async(): Promise<void> => 
  {
    const loading = await this.loadingController.create({
      message: 'Mise à jour'
    })

    await loading.present()

    const activateUpdate = this.swUpdate.activateUpdate()

    activateUpdate.then( (): void => 
    {
      loading.dismiss()
      document.location.reload()
    })
  }

  /**
  * Ask if get update now or later
  */
  private presentUpdateAlert = async(): Promise<void> => 
  {
    const alert = await this.alertController.create({
      header: 'Mise à jour',
      message: 'Une nouvelle mise à jour est disponible, voulez vous faire la mise à jour maintenant ?',
      buttons: [{
        text: 'Plus tard',
        role: 'cancel',
        cssClass: 'secondary'
      }, 
      {
        text: 'Mettre à jour',
        handler: (): void => 
        {
          this.updateToLatest()
        }
      }]
    })

    await alert.present()
  }
}
