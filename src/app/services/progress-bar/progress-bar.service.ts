import { Injectable } from '@angular/core'

import { SpendingsService, Spending } from '../spendings/spendings.service'
import { EarningsService, Earning } from '../earnings/earnings.service'
import { LeviesService, Levy } from '../levies/levies.service'
import { CoreService } from '../core/core.service'

@Injectable({
   providedIn: 'root'
})
export class ProgressBarService 
{
   constructor(
      private spendingsService: SpendingsService,
      private earningsService: EarningsService,
      private leviesService: LeviesService,
      private coreService: CoreService
   ) { }

   /**
   * Get current core date progress bar values
   * @return {Promise<ProgressBarValues>}
   */
   public getMonthProgressBarValuesForCoreCurrentDate = async(): Promise<ProgressBarValues> =>
   {
      const coreCurrentMonth: number = this.coreService.getCurrentMonth()
      const coreCurrementYear: number = this.coreService.getCurrentYear()

      let progressBarValues: ProgressBarValues = new ProgressBarValues()

      progressBarValues.earningsSum = await this.earningsService.getEarningsSumForMonth(coreCurrentMonth, coreCurrementYear)
      progressBarValues.spendingsSum = await this.spendingsService.getSpendingsSumForMonth(coreCurrentMonth, coreCurrementYear)
      progressBarValues.spendingsSum += await this.leviesService.getLeviesSumForMonth(coreCurrentMonth, coreCurrementYear)

      progressBarValues.progressPercentage = (progressBarValues.spendingsSum/progressBarValues.earningsSum)
      
      progressBarValues.progressPercentage = parseFloat(progressBarValues.progressPercentage.toFixed(2))
      if( progressBarValues.progressPercentage > 1 )
         progressBarValues.progressPercentage = 1

      progressBarValues.available = progressBarValues.earningsSum - progressBarValues.spendingsSum

      return progressBarValues
   }
   
}

export class ProgressBarValues 
{
   constructor(
      public earningsSum        : number = 0,
      public spendingsSum       : number = 0,
      public progressPercentage : number = 0,
      public available          : number = 0
   ){}

}
